public class Ejercicio3Password {
	 
	    public static void main(String[] args) {
	    	Ejercicio3Password password1 = new Ejercicio3Password();
	    	Ejercicio3Password password2 = new Ejercicio3Password();
			System.out.println(password1.toString());
			System.out.println(password2.toString());
	    }

public class Ejercicio3Password4 {

		private int longitud=8;
		private String contrasea;
		String caracteres[]= {"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","","O","P","Q","R","S","T","U","V","X","X","Y","Z"};
		
		
		protected Ejercicio3Password4() {
			this.contrasea="";
		}
		
		protected Ejercicio3Password4(int longitud) {
			this.longitud=longitud;
			this.contrasea=getPass();
		}

		protected int getLongitud() {
			return longitud;
		}

		protected void setLongitud(int longitud) {
			this.longitud = longitud;
		}

		protected String getContrasea() {
			return contrasea;
		}

		protected void setContrasea(String contrasea) {
			this.contrasea = contrasea;
		}
		
		  public String getPass (){
			  
		        String pass="";
		        
		        for (int i=0;i<longitud;i++){
		        	int letrasPass = (int)(Math.random()*64);
		                	pass+=caracteres[letrasPass];
		        }
		        
		        return pass;
		    }

		@Override
		public String toString() {
			return "Password [longitud=" + longitud + ", contrasea=" + contrasea + "]";
		}
}
		  
		  
	}

