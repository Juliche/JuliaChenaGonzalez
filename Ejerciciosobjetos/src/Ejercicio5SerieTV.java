
	public class Ejercicio5SerieTV  {
		 
	    public static void main(String[] args) {
	    	Ejercicio5SerieTV serie1 = new Ejercicio5SerieTV();
	    	Ejercicio5SerieTV serie2 = new Ejercicio5SerieTV();
	    	Ejercicio5SerieTV serie3 = new Ejercicio5SerieTV();
			System.out.println(serie1.toString());
			System.out.println(serie2.toString());
			System.out.println(serie3.toString());
			
	    }
	    
public class Ejercicio5SerieTV4 {

	String titulo;
	int numTemporadas=3;
	boolean entregado=false;
	String genero;
	String creador;
	
	protected String getTitulo() {
		return titulo;
	}
	protected void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	protected int getNumTemporadas() {
		return numTemporadas;
	}
	protected void setNumTemporadas(int numTemporadas) {
		this.numTemporadas = numTemporadas;
	}
	protected boolean isEntregado() {
		return entregado;
	}
	protected void setEntregado(boolean entregado) {
		this.entregado = entregado;
	}
	protected String getGenero() {
		return genero;
	}
	protected void setGenero(String genero) {
		this.genero = genero;
	}
	protected String getCreador() {
		return creador;
	}
	protected void setCreador(String creador) {
		this.creador = creador;
	}
	
	protected Ejercicio5SerieTV4() {
		this.titulo="";
		this.genero="";
		this.creador="";
	}
	
	protected Ejercicio5SerieTV4(String titulo, String creador) {
		this.titulo=titulo;
		this.genero="";
		this.creador=creador;
	}
	
	protected Ejercicio5SerieTV4(String titulo, String creador, String genero) {
		this.titulo=titulo;
		this.genero=genero;
		this.creador=creador;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numTemporadas=" + numTemporadas + ", entregado=" + entregado + ", genero="
				+ genero + ", creador=" + creador + "]";
	}
	

}
}

